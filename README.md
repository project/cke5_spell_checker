WProofreader SDK is an AI-driven, multi-language text correction tool. Spelling, grammar, and punctuation suggestions appear on hover as you type or in a separate dialog aggregating all mistakes and replacement suggestions in one place.

This is a premium feature that is additionally payable on top of CKEditor 5 commercial license fee and delivered by our partner, WebSpellChecker. Contact us if you have any feedback or questions.

You can report any issues in the WebSpellChecker GitHub repository.